RMNT Toolkit

A simple script for Windows-based operating systems to prevent malicious programs (for example : Shortcut Virus) to run.
This will help you to clean and decrease the possibility to get infected again in the future.
This project currently focused to maintain it's power as a script-based programs.
But there's a plan to make another interesting GUI version in the future.
This is an open-source project, but every little donation is welcome.


System Requirements

Actually, this script is compatible for those who uses Microsoft Windows 7 up to the latest version of Microsoft Windows 10.
However, backward compatibility for Microsoft Windows Vista & Microsoft Windows XP is not guaranteed tough.
To run this script, you only need a default command-prompt, no-need to install anything.
And remember that all the command is fully compatible for any processor architecture, no matter which one you have.


FAQ

1. How to start ?
--> Double click on the file named 'RMNT-Toolkit.bat', if it closes by itself and no message popped up, read number 5 below.

2. Do I need to disable my antivirus program ?
--> Sure, you need to ask your guards to take a rest for a while.

3. Your script is detected as a malicious code by my antivirus program ?
--> Don't worry, it is safe. You can review the code first with notepad or alike before executes them.
    Or, maybe you can verify it's hashes first, it should match the one i have made here : https://rnnharris.thisistap.com/RMNT-Toolkit.sha1

4. Is this for x86 or x64 version of Microsoft Windows OSes ?
--> Both is supported.

5. Why script-based programs ?
--> I don't have enough time to make the GUI version right now.

6. The program closes by itself at first, what to do ?
--> You can try to run 'RMNT-Toolkit-Alt.bat' as an Administrator.

7. How can I get you contacted with ?
--> I have a Twitter Account, you can send me a request by DM to get my email address.

8. I'm interested to contribute to this project, is there any contributing guide to follow ?
--> Let's have a chat to talk about it as a team.

9. How to donate ?
--> By Paypal or Bitcoin. Ask the account address by DM to my Twitter.


Special Thanks

1. www.techgainer.com - Source Code to run script-based program as an admin automatically
